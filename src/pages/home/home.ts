import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Http} from "@angular/http";
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  pages:any = [];
  id:any = 1;
  endOfList:boolean = false;
  constructor(public navCtrl: NavController, public http:Http) {

  }

  ngOnInit(){
    this.http.get('https://jsonplaceholder.typicode.com/posts/1').map(res => res.json()).subscribe(data => {
      console.log(JSON.stringify(data,null,4));
      this.pages = data;
    });
  }

  RetriveData(id){
    if(id == 1){
      this.endOfList = true;
      this.id = 1;
      alert('Start of list');
    }else{
      this.http.get('https://jsonplaceholder.typicode.com/posts/'+id).map(res => res.json()).subscribe(data => {
        console.log(JSON.stringify(data,null,4));
        this.pages = data;
      });
    }
  }

  left(){
    this.RetriveData(this.id-1);
    this.id = this.id-1;
  }

  right(){
    this.RetriveData(this.id+1);
    this.id = this.id+1;
  }

  swipeEvent($event) : Promise<any>{
    return new Promise((resolve) => {
      setTimeout(() => {
        if($event.direction == '4'){
          this.RetriveData(this.id-1);
          this.id = this.id-1;
        }
        else if($event.direction == '2'){
          this.RetriveData(this.id+1);
          this.id = this.id+1;
        }
        console.log('Async operation has ended');
        resolve();
      }, 500);
    })
  }

}
